<?php
    $server_name = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "uasspatu";

    $connection = mysqli_connect($server_name,$db_username,$db_password,$db_name);
    $dbconfig = mysqli_select_db($connection,$db_name);    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
    integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <title>uas spatu</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <br><h3 class="text-center">Daftar Sepatu</h3><br>
    <table class="m-auto content-table">
        <?php
            $query = "select m.id_spatu, m.nama_spatu, m.harga, m.gender_spatu, m.photos, p.nama_merk
            from daftarspatu m, merkspatu p
            where m.id_merk=p.id_merk";
            $query_run = mysqli_query($connection, $query);
        ?>
        <thead>
            <tr>
            <th>id sepatu</th>
            <th>Nama sepatu</th>
            <th>Harga</th>
            <th>Jenis sepatu</th>
            <th>Merk</th>
            <th>Foto</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(mysqli_num_rows($query_run) > 0)
                {
                    while($row = mysqli_fetch_assoc($query_run))
                    {
                        ?>
            <tr>
                <td width="150"><?php echo $row['id_spatu']; ?></td>
                <td width="150"><?php echo $row['nama_spatu']; ?></td>
                <td width="150"><?php echo $row['harga']; ?></td>
                <td width="200"><?php echo $row['gender_spatu']; ?></td>
                <td width="150"><?php echo $row['nama_merk']; ?></td>
                <td width="150"><img src="images/<?php echo $row['photos']; ?>" width="100"></td>
            </tr>
            <?php
                    }
                }
                
                else
                {
                    echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
                }
                ?>
        </tbody>
    </table>


    <br><h3 class="text-center">Data Sepatu</h3>
    <table class="m-auto content-table">
        <?php
            $query2 = "select m.id_merk, m.nama_merk, p.brand
            from merkspatu m, brandspatu p
            where m.id_brand=p.id_brand";
            $query_run2 = mysqli_query($connection, $query2);
        ?>
        <thead>
            <tr>
            <th>Id Merk</th>
            <th>Nama Merk</th>
            <th>Brand</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(mysqli_num_rows($query_run2) > 0)
                {
                    while($row2 = mysqli_fetch_assoc($query_run2))
                    {
                        ?>
            <tr>
                <td width="150"><?php echo $row2['id_merk']; ?></td>
                <td width="200"><?php echo $row2['nama_merk']; ?></td>
                <td width="200"><?php echo $row2['brand']; ?></td>
            </tr>
            <?php
                    }
                }
                
                else
                {
                    echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
                }
                ?>
        </tbody>
    </table>
    <br>

    <br><h3 class="text-center">Brand Sepatu</h3>
    <table class="m-auto content-table">
        <?php
            $query2 = "select * from brandspatu";
            $query_run2 = mysqli_query($connection, $query2);
        ?>
        <thead>
            <tr>
            <th>Id Brand</th>
            <th>Brand</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(mysqli_num_rows($query_run2) > 0)
                {
                    while($row2 = mysqli_fetch_assoc($query_run2))
                    {
                        ?>
            <tr>
                <td width="150"><?php echo $row2['id_brand']; ?></td>
                <td width="200"><?php echo $row2['brand']; ?></td>
            </tr>
            <?php
                    }
                }
                
                else
                {
                    echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
                }
                ?>
        </tbody>
    </table>
    <br>
</body>
</html>